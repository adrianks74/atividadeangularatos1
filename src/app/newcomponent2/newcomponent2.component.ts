import { Component, OnInit } from '@angular/core';
import { Pessoa } from '../models/Pessoa.model';

@Component({
  selector: 'app-newcomponent2',
  templateUrl: './newcomponent2.component.html',
  styleUrls: ['./newcomponent2.component.css']
})
export class Newcomponent2Component implements OnInit {

  pessoas =
  [
    new Pessoa("Fabricio" , "Professor" ),
    new Pessoa("Fabricio1", "Professor1"),
    new Pessoa("Fabricio2", "Professor2"),
    new Pessoa("Fabricio3", "Professor3"),
    new Pessoa("Fabricio4", "Professor4"),
    new Pessoa("Fabricio5", "Professor5"),
    new Pessoa("Fabricio6", "Professor6"),
    new Pessoa("Fabricio7", "Professor7"),
    new Pessoa("Fabricio8", "Professor8")
  ];

  constructor() { }

  ngOnInit() {


  }

}
