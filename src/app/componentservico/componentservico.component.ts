import { Component, OnInit } from '@angular/core';
import { Album } from '../models/Album.model';
import { Servico1Service } from '../services/servico1.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-componentservico',
  templateUrl: './componentservico.component.html',
  styleUrls: ['./componentservico.component.css']
})
export class ComponentservicoComponent implements OnInit {


  error:any;
  albums:Album;

  constructor( private router: Router, private photoServico:Servico1Service)
  {
    this.getter();
  }

  ngOnInit(): void {
  }

  getter()
  {
    this.photoServico.getAlbums().subscribe
    (
      (data:Album) =>
      {
        this.albums = data;
      },
      (error:any) =>
      {
        this.error = error;
      }
    );

  }

  onClick( a:Album )
  {
    this.router.navigateByUrl("/fotos?albumId="+a.id);
    console.log("Clicou no album "+a.id);
  }

}
