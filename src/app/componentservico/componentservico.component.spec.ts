import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentservicoComponent } from './componentservico.component';

describe('ComponentservicoComponent', () => {
  let component: ComponentservicoComponent;
  let fixture: ComponentFixture<ComponentservicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentservicoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentservicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
