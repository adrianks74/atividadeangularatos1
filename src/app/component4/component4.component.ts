import { Component, OnInit, NgModule } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Pessoa } from '../models/Pessoa.model';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';
import { createElementCssSelector } from '@angular/compiler';

@Component({
  selector: 'app-component4',
  templateUrl: './component4.component.html',
  styleUrls: ['./component4.component.css']
})
export class Component4Component implements OnInit {

  pessoas =
  [
    new Pessoa("Fabricio" , "Professor" ),
    new Pessoa("Fabricio1", "Professor1"),
    new Pessoa("Fabricio2", "Professor2"),
    new Pessoa("Fabricio3", "Professor3"),
    new Pessoa("Fabricio4", "Professor4"),
    new Pessoa("Fabricio5", "Professor5"),
    new Pessoa("Fabricio6", "Professor6"),
    new Pessoa("Fabricio7", "Professor7"),
    new Pessoa("Fabricio8", "Professor8")
  ];


  selecionado = null;

  editando = null;
  nome = null;
  papel = null;


  cadastro = new FormGroup(

    {
      nome: new FormControl(''),
      papel: new FormControl('')
    }
  );

  exclude = new FormGroup(
    {
      delnome: new FormControl('')
    }
  );

  selecionar(pessoa)
  {
    this.selecionado = pessoa;
  }

  onSubmit()
  {
    if(this.editando != null)
    {
      this.editando.nome = this.nome;
      this.editando.papel = this.papel;
    }
    else
    {
      const dados = new Pessoa(this.cadastro.value.nome, this.cadastro.value.papel);
      this.pessoas.push(dados);
    }

    this.resetar();

  }

  onExclude()
  {
    let deleteNome = this.exclude.value.delnome;

    if(confirm("Tem certeza de que quer deletar pessoa = "+deleteNome))
    {
      for(let i = 0; i < this.pessoas.length; i++)
        if( this.pessoas[i].nome === deleteNome )
          {
            this.pessoas.splice(i,1);
            return;
          }
    }

    confirm("Pessoa não existe para deletar");
  }

  resetar()
  {
    this.nome = null;
    this.papel = null;
    this.editando = null;
  }

  editar(pessoa)
  {
    this.editando = pessoa;
    this.nome = pessoa.nome;
    this.papel = pessoa.papel;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
