import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-newcomponent',
  templateUrl: './newcomponent.component.html',
  styleUrls: ['./newcomponent.component.css']
})
export class NewcomponentComponent implements OnInit
{

  numero:number = 0;

  pessoas =
  [
    {nome:"Fab" , papel:"Professor1"},
    {nome:"Fab1", papel:"Professor2"},
    {nome:"Fab2", papel:"Professor3"},
    {nome:"Fab3", papel:"Professor4"},
    {nome:"Fab4", papel:"Professor5"},
    {nome:"Fab5", papel:"Professor6"},
    {nome:"Fab6", papel:"Professor7"},
    {nome:"Fab7", papel:"Professor8"},
  ];

  constructor() { }

  ngOnInit(): void
  {
  }

}
