import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewcomponentComponent } from './newcomponent/newcomponent.component';
import { Newcomponent2Component } from './newcomponent2/newcomponent2.component';
import { ComponentComponent } from './component/component.component';
import { Component4Component } from './component4/component4.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ComponentservicoComponent } from './componentservico/componentservico.component';
import { HttpClientModule } from '@angular/common/http';
import { ComponentphotosComponent } from './componentphotos/componentphotos.component';

@NgModule({
  declarations: [
    AppComponent,
    NewcomponentComponent,
      Newcomponent2Component,
      ComponentComponent,
      Component4Component,
      ComponentservicoComponent,
      ComponentphotosComponent
   ],

  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
