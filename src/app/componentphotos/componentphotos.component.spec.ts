import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentphotosComponent } from './componentphotos.component';

describe('ComponentphotosComponent', () => {
  let component: ComponentphotosComponent;
  let fixture: ComponentFixture<ComponentphotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentphotosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentphotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
