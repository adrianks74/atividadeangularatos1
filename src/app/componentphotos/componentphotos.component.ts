import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Photo } from '../models/Photo.model';
import { Servico1Service } from '../services/servico1.service';

@Component({
  selector: 'app-componentphotos',
  templateUrl: './componentphotos.component.html',
  styleUrls: ['./componentphotos.component.css']
})
export class ComponentphotosComponent implements OnInit {

  albumID :number = -1;
  photos:Photo;
  error:any;

  constructor( private route: ActivatedRoute, private photoServico:Servico1Service)
  {
    this.route.queryParams.subscribe(
      (params) =>
      {
        this.albumID = params['albumId'];
        this.getter();
      }
    )
  }

  ngOnInit(): void {

  }

  getter()
  {
    this.photoServico.getPhotos( this.albumID ).subscribe
    (
      (data:Photo) =>
      {
        this.photos = data;
        console.log('o que retornou em data:', data);
        console.log('variavel photos:', this.photos);
      },
      (error:any) =>
      {
        this.error = error;
        console.error('o que retornou em data:', this.error);
      }
    );

  }

}
