import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewcomponentComponent } from './newcomponent/newcomponent.component';
import { Newcomponent2Component } from './newcomponent2/newcomponent2.component';
import { Component4Component } from './component4/component4.component';
import { ComponentservicoComponent } from './componentservico/componentservico.component';
import { ComponentphotosComponent } from './componentphotos/componentphotos.component';

const routes: Routes = [
  {path:'newcomponent', component:NewcomponentComponent},
  {path:'newcomponent2', component:Newcomponent2Component},
  {path:'newcomponent4', component:Component4Component},
  {path:'albums', component:ComponentservicoComponent},
  {path:'fotos', component:ComponentphotosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
