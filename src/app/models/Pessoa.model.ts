export class Pessoa
{
  nome:String;
  papel:String;

  constructor(nome:String, papel?:String)
  {
    this.nome = nome;
    this.papel = papel;
  }
}
